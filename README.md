This repository contains the automated tests to meet the Acceptance Criterea for Kennards Self Storage Box Shop user stories.

#How do I execute?

Pre-Condition - Python3 and Selenium is installed.

The automated tests can be executed by running the following command in shell in the containing folder:
python test_pages.py

If an HTML execution report is required, the tests can be executed in shell by running the following command:
python test_HTMLTestRunner.py > result.html
