# -*- coding: utf8 -*-
# Test Cases added here for better maintainibility


def test_cases(number):
    return us1_testCases[number]


us1_testCases = [
    # [user_story, description]
    ['US1', 'In Box It Page, when user increments item count of Jumbo Box Heavy Duty, input field should be updated'],
    ['US1', 'In Box It Page, when user decrements item count of Jumbo Box Heavy Duty, input field should be updated'],
    ['US2', 'In Box It Page, when user adds items to cart, the cart should be updated with a confirmation'],
    ['US3', 'In Shopping cart Page, when user removes a product, the cart should be updated with confirmation and total price should be updated'],
    ['US4', 'In Shopping cart Page, when user triggers Checkout, the checkout page should be displayed']
]
