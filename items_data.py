# test data is stored here

items = [
	{"product_name": "Jumbo Box Heavy Duty","item_count": "5", "price": 6.40},
    	{"product_name": "Jumbo Box Light Duty","item_count": "25", "price": 4.6 }
]

def get_product(name):
	try:
		return (item for item in items if item["product_name"] == name).next()
	except:
		print "\n     Item %s is not defined, enter a valid item.\n" %name
