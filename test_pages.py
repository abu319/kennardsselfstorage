import unittest
from selenium import webdriver
from pages import *
from test_cases import test_cases


class TestPages(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.driver.get("https://m.kss.com.au/boxshop/")

    def test_BoxIt_increment_item_count(self):
        print "\n" + str(test_cases(0))
        page = BoxItPage(self.driver)
        self.assertTrue(page.check_page_loaded(), 'Box It page not loaded')
        item_count = page.get_input_item_count('Jumbo Box Heavy Duty')
        self.assertEqual(str(item_count), str(1), 'Default item count is not 1')
        item_count = page.input_item_count('Jumbo Box Heavy Duty')
        expected_item_count = items_data.get_product('Jumbo Box Heavy Duty')["item_count"]
        self.assertTrue(str(item_count) == expected_item_count, 'Item count is not updated')
        page.increment_item_count('Jumbo Box Heavy Duty')
        self.assertEqual(int(page.get_input_item_count('Jumbo Box Heavy Duty')), int(expected_item_count)+1, 'Item count not incremented')

    def test_BoxIt_decrement_item_count(self):
        print "\n" + str(test_cases(1))
        page = BoxItPage(self.driver)
        self.assertTrue(page.check_page_loaded(), 'Box It page not loaded')
        item_count = page.get_input_item_count('Jumbo Box Heavy Duty')
        self.assertTrue(str(item_count) == str(1))
        item_count = page.input_item_count('Jumbo Box Heavy Duty')
        expected_item_count = items_data.get_product('Jumbo Box Heavy Duty')["item_count"]
        self.assertTrue(str(item_count) == expected_item_count, 'Item count not equal to expected')
        page.decrement_item_count('Jumbo Box Heavy Duty')
        self.assertEqual(int(page.get_input_item_count('Jumbo Box Heavy Duty')), int(expected_item_count)-1, 'Item count not decremented')


    def test_add_items_to_cart(self):
        print "\n" + str(test_cases(2))
        page = BoxItPage(self.driver)
        driver = self.driver
        item_count = page.input_item_count('Jumbo Box Heavy Duty')
        obj_msg = page.add_item_to_cart('Jumbo Box Heavy Duty')
        expected_item_count = items_data.get_product('Jumbo Box Heavy Duty')["item_count"]
        self.assertTrue(page.get_success_message(obj_msg)[0] == expected_item_count)
        self.assertTrue(page.get_success_message(obj_msg)[1] == 'Jumbo Box Heavy Duty')
        self.assertTrue(page.get_basket_count(driver) == expected_item_count)
        self.assertTrue(page.get_continue_shopping('Jumbo Box Heavy Duty').text == 'CONTINUE SHOPPING')


    def test_remove_product(self):
        print "\n" + str(test_cases(3))
        page = BoxItPage(self.driver)
        driver = self.driver
        page.input_item_count('Jumbo Box Heavy Duty')
        page.input_item_count('Jumbo Box Light Duty')
        page.add_item_to_cart('Jumbo Box Heavy Duty')
        time.sleep(2)
        page.add_item_to_cart('Jumbo Box Light Duty')
        time.sleep(2)
        cart_page = page.goto_cart(driver)
        time.sleep(3)
        self.assertTrue(cart_page.check_page_loaded(), 'Cart page not loaded')
        cart_page.remove_product('Jumbo Box Heavy Duty')
        cart_page = CartPage(self.driver)
        self.assertEqual(cart_page.get_product_count(), 1, 'Product is not removed from cart')
        total_price = cart_page.get_total_price(driver)
        expected_price = int(items_data.get_product('Jumbo Box Light Duty')["item_count"])*(items_data.get_product('Jumbo Box Light Duty')["price"])
        self.assertEqual(float(total_price.split(' ')[1][1:]), float(round(expected_price)), 'Total price is not updated')

    def test_checkout_products(self):
        print "\n" + str(test_cases(4))
        page = BoxItPage(self.driver)
        driver = self.driver
        page.input_item_count('Jumbo Box Heavy Duty')
        page.add_item_to_cart('Jumbo Box Heavy Duty')
        time.sleep(2)
        cart_page = page.goto_cart(driver)
        time.sleep(3)
        self.assertTrue(cart_page.check_page_loaded())
        product_qty_price = cart_page.get_product_qty_price('Jumbo Box Heavy Duty')
        product_qty = product_qty_price[0]
        self.assertEqual(product_qty, items_data.get_product('Jumbo Box Heavy Duty')["item_count"],'Product quantity not as expected')
        product_price = product_qty_price[1]
        expected_price = int(items_data.get_product('Jumbo Box Heavy Duty')["item_count"]) * items_data.get_product('Jumbo Box Heavy Duty')["price"]
        self.assertEqual(float(product_price[1:]), expected_price, 'Product price is not as expected')
        total_price = cart_page.get_total_price(driver)
        self.assertEqual(float(total_price.split(' ')[1][1:]), float(round(expected_price)), 'Total price is not updated')
        checkout_page = cart_page.checkout(driver)
        self.assertTrue(checkout_page.check_page_loaded())


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPages)
    unittest.TextTestRunner(verbosity=2).run(suite)

