from selenium.webdriver.support.ui import Select

import time
import items_data
from Page import Page
from locator_maps import *

# All Page objects implementation.
#***********************************************************************************************************************
class BoxItPage(Page):
    def check_page_loaded(self):
        return self.find_element(*BoxItPageLocatars.PAGE_TITLE).text == 'Box it'

    def get_input_item(self, item):
        items_grid = self.find_element(*BoxItPageLocatars.ITEMS_GRID)
        items_list = self.find_child_elements(items_grid, *BoxItPageLocatars.ITEMS_LIST)
        for i in range(len(items_list)):
            item_title = self.find_child_element(items_list[i], *BoxItPageLocatars.ITEM_TITLE)
            if (item_title.text == item):
                return items_list[i]

    def input_item_count(self, item):
        obj_item = self.get_input_item(item)
        input_text = self.find_child_element(obj_item, *BoxItPageLocatars.ITEM_COUNT)
        input_text.clear()
        input_text.send_keys(items_data.get_product(item)["item_count"])
        time.sleep(1)
        return input_text.get_attribute('value')

    def get_input_item_count(self, item):
        obj_item = self.get_input_item(item)
        item_title = self.find_child_element(obj_item, *BoxItPageLocatars.ITEM_COUNT)
        return item_title.get_attribute('value')

    def increment_item_count(self, item):
        obj_item = self.get_input_item(item)
        btn_increment = self.find_child_element(obj_item, *BoxItPageLocatars.BTN_INCREMENT)
        btn_increment.click()

    def decrement_item_count(self, item):
        obj_item = self.get_input_item(item)
        btn_increment = self.find_child_element(obj_item, *BoxItPageLocatars.BTN_DECREMENT)
        btn_increment.click()

    def add_item_to_cart(self, item):
        obj_item = self.get_input_item(item)
        btn_add_to_cart = self.find_child_element(obj_item, *BoxItPageLocatars.BTN_ADD_TO_CART)
        btn_add_to_cart.click()
        time.sleep(3)
        return self.find_child_element(obj_item, *BoxItPageLocatars.ITEM_ADD_SUCCESS_MSG)

    def get_success_message(self, obj_msg):
        item_added = self.find_child_element(obj_msg, *BoxItPageLocatars.ITEM_ADD_SUCCESS_COUNT).text
        item_name =  self.find_child_element(obj_msg, *BoxItPageLocatars.ITEM_ADD_SUCCESS_NAME).text
        return (item_added, item_name, obj_msg.text )

    def get_basket_count(self, driver):
        return self.find_child_element(driver, *BoxItPageLocatars.BASKET_COUNT).text

    def get_continue_shopping(self, item):
        obj_item = self.get_input_item(item)
        return self.find_child_element(obj_item, *BoxItPageLocatars.BTN_CONTINUE_SHOP)

    def goto_cart(self, driver):
        btn_basket = self.find_child_element(driver, *BoxItPageLocatars.BASKET_TOGGLE)
        btn_basket.click()
        return CartPage(driver)


#***********************************************************************************************************************
class CartPage(Page):
    def check_page_loaded(self):
        return self.find_element(*ShoppingCartPageLocatars.TITLE).text == 'Shopping Basket'

    def get_product_row(self, product):
        basket_items = self.find_elements(*ShoppingCartPageLocatars.BASKET_ITEMS)
        for i in range(len(basket_items)):
            product_name = self.find_child_element(basket_items[i], *ShoppingCartPageLocatars.PRODUCT_NAME).text
            if (product_name == product):
                return basket_items[i]

    def remove_product(self, product):
        basket_row = self.get_product_row(product)
        btn_remove = self.find_child_element(basket_row, *ShoppingCartPageLocatars.PRODUCT_REMOVE)
        btn_remove.click()
        time.sleep(5)
        return

    def get_product_count(self):
        basket_items = self.find_elements(*ShoppingCartPageLocatars.BASKET_ITEMS)
        return len(basket_items)

    def get_product_qty_price(self, product):
        basket_row = self.get_product_row(product)
        product_qty = self.find_child_element(basket_row, *ShoppingCartPageLocatars.PRODUCT_QTY)
        product_price = self.find_child_element(basket_row, *ShoppingCartPageLocatars.PRODUCT_PRICE)
        return (product_qty.get_attribute('value'), product_price.text)

    def get_total_price(self, driver):
        basket_summary = self.find_child_element(driver,*ShoppingCartPageLocatars.BASKET_SUMMARY )
        return self.find_child_element(basket_summary, *ShoppingCartPageLocatars.TOTAL_AMOUNT).text

    def checkout(self, driver):
        btn_check = self.find_child_element(driver,*ShoppingCartPageLocatars.BTN_CHECKUT )
        btn_check.click()
        time.sleep(3)
        return CheckoutPage(driver)

#***********************************************************************************************************************
class CheckoutPage(Page):
    def check_page_loaded(self):
        return self.find_element(*CheckoutPageLocatars.TITLE).text == 'Checkout'

