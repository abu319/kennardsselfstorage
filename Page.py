from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# The Page Class serves as the parent class, each HTML page corresponds to anther Page object class inherited from Page

class Page(object):
    def __init__(self, driver, base_url='http://www.google.com/'):
        self.base_url = base_url
        self.driver = driver
        self.timeout = 30

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def find_elements(self, *locator):
        return self.driver.find_elements(*locator)

    def find_child_element(self, parent, *locator):
        self.driver = parent
        return self.driver.find_element(*locator)

    def find_child_elements(self, parent, *locator):
        self.driver = parent
        return self.driver.find_elements(*locator)

    def open(self, url):
        url = self.base_url + url
        self.driver.get(url)

    def get_title(self):
        return self.driver.title

