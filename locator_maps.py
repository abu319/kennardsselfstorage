from selenium.webdriver.common.by import By

# Web Element Locators in one page for better maintenence

class BoxItPageLocatars(object):
  PAGE_TITLE              = (By.XPATH, '//*[@id="page-header"]/a')
  ITEMS_GRID              = (By.XPATH, '//*[@id="boxshop"]/div[2]/div/div[4]')
  ITEMS_LIST              = (By.CLASS_NAME, 'boxshop-item')
  ITEM_TITLE              = (By.CLASS_NAME, 'boxshop-item-title')
  ITEM_COUNT              = (By.XPATH, './/div/div[3]/input')
  BTN_INCREMENT           = (By.XPATH, './/div/div[3]/a[2]')
  BTN_DECREMENT           = (By.XPATH, './/div/div[3]/a[1]')
  BTN_ADD_TO_CART         = (By.XPATH, './/div/div[3]/button')
  BASKET_COUNT            = (By.CLASS_NAME, 'basket-count')
  ITEM_ADD_SUCCESS_COUNT  = (By.XPATH, './/h3')
  ITEM_ADD_SUCCESS_NAME   = (By.XPATH, './/p')
  ITEM_ADD_SUCCESS_MSG    = (By.XPATH, './/div/div[4]/div')
  BTN_CONTINUE_SHOP       = (By.CLASS_NAME, 'boxshop-item-continue')
  BASKET_TOGGLE           = (By.CLASS_NAME, 'basket-toggle')


class ShoppingCartPageLocatars(object):
  TITLE          = (By.XPATH, '//*[@id="basket-contents"]/div/h2/strong')
  BASKET_ITEMS   = (By.CLASS_NAME, 'basket-item')
  PRODUCT_NAME   = (By.XPATH, './/td[2]')
  PRODUCT_QTY    = (By.XPATH, './/td[3]/div/input')
  PRODUCT_PRICE  = (By.XPATH, './/td[4]')
  PRODUCT_REMOVE = (By.XPATH, './/td[5]/a')
  BASKET_SUMMARY = (By.CLASS_NAME, 'basket-summary')
  TOTAL_AMOUNT   = (By.XPATH, './/tbody/tr[1]/td[3]/p')
  BTN_CHECKUT    = (By.XPATH, '//*[@id="basket-contents"]/div/table/tbody/tr[2]/td[3]/button')

class CheckoutPageLocatars(object):
  TITLE = (By.XPATH, '//*[@id="page-header"]/strong')





